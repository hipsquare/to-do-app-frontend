export interface ToDo {
  id: number;
  Name?: string;
  Done?: boolean;
}
